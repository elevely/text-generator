import re
import operator
from collections import defaultdict
import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--input-dir', default="./", help='Путь для обработки')
parser.add_argument('--model', default="./", help='Путь для сохранения модели')

args = parser.parse_args()

dictionary = defaultdict(int)
available_symbols = re.compile(u'[а-яА-Я0-9-]+|[.,:;?!]+')


def get_dir_files():
    path_f = []
    for d, dirs, files in os.walk(args.input_dir):
        for f in files:
            path = os.path.join(d, f)
            path_f.append(path)
    return path_f


def gen_lines_from_stdin():
    print('После ввода текста в новой строке введите: [end]')
    while True:
        try:
            line = input()
            if line == '[end]':
                break
            yield line.lower()
        except Exception:
            yield line.lower()


def gen_lines_from_dir(adress):
    for i in adress:
        data = open(i)
        for line in data:
            yield line.lower()


def gen_tokens(lines): #создает генератор всех слов всех строк
    for line in lines:
        for token in available_symbols.findall(line):
            yield token


def gen_bi(tokens): #создает генератор биграмм
    fir_tok = '$'
    for sec_tok in tokens:
        yield fir_tok, sec_tok
        if sec_tok in '.!?':
            yield sec_tok, '$'
            fir_tok = '$'
        else:
            fir_tok = sec_tok

try:
    adress = get_dir_files(args)
    print(adress)
    lines = gen_lines(adress)
    tokens = gen_tokens(lines)
    bi = gen_bi(tokens)

    processed_bi = defaultdict(lambda: 0.0)
    words = defaultdict(lambda: 0.0)
    for fir_tok, sec_tok in bi:
        processed_bi[fir_tok, sec_tok] += 1
        words[fir_tok] += 1

    right_per_bi = defaultdict(list)

    for (fir_tok, sec_tok), per in processed_bi.items():
        right_per_bi[fir_tok].append([])
        right_per_bi[fir_tok].append([])
        right_per_bi[fir_tok][0].append(sec_tok)
        right_per_bi[fir_tok][1].append(per / words[fir_tok])

    book = open(args.model, 'w')
    for i in right_per_bi:
        book.write(i + "@")
        le = len(right_per_bi[i][0]) - 1
        for j in range(le):
            book.write(str(right_per_bi[i][0][j]) + '&')
        book.write(str(right_per_bi[i][0][le]) + '^')
        for j in range(len(right_per_bi[i][1]) - 1):
            book.write(str(right_per_bi[i][1][j]) + '&')
        book.write(str(right_per_bi[i][1][le]) + '\n')
    print('status: success')
except Exception:

    print('status: failed')
raise
