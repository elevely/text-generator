import operator
from collections import defaultdict
import argparse
import numpy

parser = argparse.ArgumentParser()
parser.add_argument('--model', default="./", help='Адрес модели для загрузки')
parser.add_argument('--length', default=100, help='Длина генерируемого текста')

args = parser.parse_args()

dictionary = defaultdict(int)


def read_model(model_name):
    file = open(args.model + '/' + model_name, 'r')
    try:
        for line in file:
            i, model[i] = line.split('@')
            model[i] = model[i].split('^')
            model[i][0] = model[i][0].split('&')
            model[i][1] = model[i][1].split('&')
            if model[i][1][-1][-1] == '\n':
                temp = model[i][1][-1][0: len(model[i][1][-1]) - 2]
                model[i][1][-1] = float(temp)
    except Exception:
        print('Failed in read_model: ', model[i][0][j], ' or ', model[i][1][j])
    else:
        return model


def gen_text(model):
    word = numpy.random.choice(model['$'][0], 1, model['$'][1])
    print(word[0].capitalize(), end='')
    temp = numpy.random.choice(model[word[0]][0], 1, model[word[0]][1])
    i = 1
    prev = ''
    while i < int(args.length):
        if temp[0] != '$':
            if prev == '$':
                print(' ' + temp[0].capitalize(), end="")
            else:
                if temp[0] in ',..."!?:;':
                    print(temp[0], end="")
                else:
                    print(' ' + temp[0], end="")
        try:
            prev = temp[0]
            temp = numpy.random.choice(model[temp[0]][0], 1, model[temp[0]][1])
        except Exception:
            prev = temp[0]
            temp = numpy.random.choice(model['$'][0], 1, model['$'][1])
        i += 1
    if temp[0] not in ',..."!?:;':
        print('.')


try:
    def read_name():
        text_for_train = input('file name: ')
        temp = ''
        for i in range(4):
            temp += text_for_train[len(text_for_train) - 4 + i]
        if temp != '.txt':
            raise Exception
        return text_for_train

    model_name = read_name()
    model = defaultdict(str)
    model = read_model(model_name)
    gen_text(model)
except Exception:
    print('failed')
    raise
